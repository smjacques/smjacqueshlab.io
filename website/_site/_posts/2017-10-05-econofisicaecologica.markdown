---
layout: post
title: "Introdução à Econofísica Ecológica Para o Decrescimento"
date: 2017-10-05 15:41
comments: true
---

Texto do pesquisador Salvador Pueyo publicado pela primeira vez em português em 2017 acerca de pesquisa que busca identificar sistemas econômicos alternativos que funcionariam entre a ordem e o caos, mas radicalmente diferentes do sistema vigente em relação à sustentabilidade ambiental e a justiça social.


<blockquote><strong><em>Salvador Pueyo
</em></strong></blockquote>
<blockquote><a href="http://www.esee2015.org/wp-content/uploads/2015/10/0657.pdf" target="_blank">Texto</a> publicado no 11<sup>o</sup> Congresso Internacional da <a href="http://www.euroecolecon.org/" target="_blank">ESEE</a>.</blockquote>
<!--more-->
<h3>1. Introdução</h3>
O presente trabalho sintetiza e atualiza as ideias apresentadas em detalhes no artigo "Ecological Econophysics for Degrowth"[^1]. À medida que os limites ambientais globais se tornam evidentes, há um crescente interesse em transformações radicais para passar de uma economia dependente do crescimento para uma forma de decrescimento que proporcione o bem-estar.

No entanto, tais transformações encontrarão grandes dificuldades, incluindo interesses já constituídos, barreiras culturais e dificuldades técnicas. O artigo pretende contribuir para uma nova teoria macroeconômica que possa abordar os problemas técnicos decorrentes do decrescimento. Argumento que, além das ideias da economia ecológica, esta teoria precisará de <em>insights</em> de estudos de sistemas complexos, especialmente da disciplina recentemente desenvolvida conhecida como Econofísica [^2]<sup>,</sup>[^3]. Portanto, ela pode ser rotulada como Econofísica Ecológica.
<br>
<h3>2. Por que Econofísica?</h3>
As alternativas radicais frequentemente enfatizam que as regras de interação entre agentes econômicos são justas em si, mas não há garantia de que deem resultados desejáveis quando aplicadas simultaneamente por um número muito grande de agentes. Os sistemas complexos apresentam frequentemente propriedades emergentes[^4], isto é, características macroscópicas que não podem ser deduzidas trivialmente a partir da dinâmica microscópica.

As propriedades emergentes nem sempre são previsíveis, mas é possível fazer prognósticos parciais com a ajuda da ciência da complexidade. Dessa forma, essa disciplina deve tornar-se uma ferramenta básica para o pensamento utópico pragmático.

Uma das principais abordagens baseadas na complexidade da teoria econômica é a Econofísica[^2]<sup>,</sup>[^3], que pode ser definida como a aplicação de conceitos e ferramentas da <a href="https://pt.wikipedia.org/wiki/F%C3%ADsica_estat%C3%ADstica" target="_blank">física estatística</a> na economia. Porém, como a economia dominante, a literatura econofísica também ignora as limitações ambientais e não se volta para a mudança do sistema econômico. Isso aponta para uma lacuna científica que necessita de um enorme esforço de pesquisa se quisermos enfrentar com êxito os principais problemas do nosso planeta.

Obviamente, precisamos de métodos adequados para evitar estes três obstáculos ao decrescimento:

• Remover partes de uma economia pode desencadear recessões descontroladas.

• A diminuição do acesso aos recursos naturais pode causar concorrência entre as necessidades básicas (por exemplo, biocombustível versus alimentos) e o consumo de luxo.

• Qualquer rearranjo profundo de um sistema complexo corre o risco de causar perda da resiliência.

Algumas das razões pelas quais os modelos de crescimento convencional não nos ajudarão são: (1) por serem modelos de equilíbrio, eles excluem fenômenos de não equilíbrio como as recessões endógenas, e (2) focando em um agente representativo, eles não são capazes de tratar de questões distributivas e excluir propriedades emergentes.

Sendo baseada na física estatística, uma abordagem econofísica é ideal para nossos objetivos, porque as flutuações econômicas e a distribuição de recursos entre os agentes são melhor descritas por distribuições estatísticas, e o trabalho da física estatística é vincular as distribuições estatísticas aos seus mecanismos subjacentes. De forma menos evidente, ela também pode contribuir muito para o problema de resiliência.
<br>
<h3>3. Enfrentando os três obstáculos</h3>
<h4>3.1 Decrescimento sem grandes recessões</h4>
As economias poderiam responder à escassez de recursos, às pressões ambientais e às políticas de austeridade de diferentes maneiras. Tanto os modelos de equilíbrio, prevendo respostas suaves, quanto os modelos tipo Meadows, prevendo o colapso, ignoram a granularidade e a modularidade da economia, o que significa que ela pode se esfacelar pouco a pouco[^1]. As economias regularmente perdem fragmentos, sob a forma de recessões. Os tamanhos das recessões apresentam uma distribuição estatística conhecida como lei de potência[^5], de forma semelhante a eventos catastróficos em muitos outros sistemas complexos, como por exemplo, incêndios florestais[^6]. As florestas respondem ao estresse ambiental com alteração nos parâmetros da distribuição da lei de potência do tamanho do fogo[^7], e esta seria a resposta mais parcimoniosa também nas economias. Porém, o colapso total é também concebível como resultado de certas políticas[^1].

Embora algumas crises sejam provavelmente inevitáveis, elas poderiam ser contornadas em parte, ao darmos à economia outros mecanismos para se reorganizar em direção a uma configuração e escala que correspondam à nova realidade ambiental (ver algumas medidas políticas experimentais na referência[^1]).
<h4>3.2 Decrescimento sem exclusão</h4>
Uma diminuição da disponibilidade de recursos resultará em um impacto mais forte nos estratos de renda mais baixa da sociedade (especialmente sobre as pessoas afetadas também por outras dimensões da desigualdade, como a desigualdade de gênero dentro de casa), podendo representar inclusive a privação do mínimo necessário para a vida. Isso só pode ser evitado se a redução dos recursos vier acompanhada pelo aumento da igualdade[^8].

Neste contexto, novamente encontramos uma distribuição de lei de potência: esta função descreve parte da distribuição de renda[^9]. As leis de potência frequentemente emergem em sistemas complexos, o que não significa que elas devam ser universais e inevitáveis. O estudo "Ecological Econophysics for Degrowth"[^1] mostra como um modelo econofísico pode ser usado para prever a distribuição de renda resultante de diferentes políticas. Este modelo não é totalmente realista e por isso precisa de mais pesquisas neste sentido. O ideal é que isso possa nos levar a métodos para obter a igualdade como uma propriedade emergente, em vez de forçá-la, controlando cada detalhe de uma economia.

A desigualdade também tem uma projeção espacial, em escalas múltiplas. É provável que ela apresente um tipo de padrão conhecido como multifractal* [^10], que sugere um caminho para prognósticos provisórios de padrões de desigualdade em todas as escalas espaciais de cenários de produção econômica decrescente, transporte decrescente e mudanças políticas.
<h3>3.3 Decrescimento sem perda de resiliência</h3>
As seções 3.1 e 3.2 mencionam leis de potência e outros padrões que não variam com a escala em variáveis-chave da economia. São características de sistemas entre ordem e caos (BOC, <em>N.T. do inglês “Between Order and Chaos”</em>)[^11]. A ciência da complexidade sugere que os sistemas complexos duradouros são necessariamente BOC[^5], e isso seria necessário para o processamento de informações complexas e para a resiliência dos sistemas.

Esse fato é crucial para as transformações que estamos antevendo, que buscam alterar, justamente, as distribuições (de recessões, de renda) cuja forma atual é indicativa de BOC. Se isso for feito ingenuamente, corremos o risco de construir um sistema que, por causa de sua estrutura, é condenado ao colapso ou a degenerar. Por exemplo, é tentador pensar que o sistema soviético possuísse um problema sistêmico de excesso de ordem. Felizmente, pode ser possível passar de um sistema econômico BOC para um sistema que seja completamente diferente, mas também seja viável e também um BOC, com alguns sintomas anteriores de BOC desaparecendo enquanto surgem outros mais benignos[^1].

Há razões para pensar que a democracia é, por si, um mecanismo BOC, o que aumenta a esperança de que podemos passar dos governos controlados pelo mercado da era da globalização para uma economia controlada democraticamente buscando a sustentabilidade global[^1].
<br>
<h3>Estratégia de Pesquisa</h3>
Aplicar a ciência da complexidade a economias não implica que tenhamos que confiar em modelos muito complexos. Precisamos aplicar modelos complexos baseados em agentes** para simular economias decrescentes alternativas, mas isso não podem constituir nossa estratégia central.

A física estatística também possui outras ferramentas. Na maioria das vezes apenas uma pequena fração da informação microscópica é relevante nas escalas macroscópicas (esta é a base do conceito bem estabelecido de classes de universalidade[^12] e da nova teoria idiossincrática[^13]). Ao tentar identificar as informações relevantes, podemos realizar previsões preliminares sem necessidade de simulações complexas.

Um estudo teórico e empírico mais aprofundado na linha da econofísica ecológica é essencial para desenvolver alternativas sólidas para uma sociedade sustentável, igualitária, democrática e agradável, e por isso convido outros cientistas a aderirem a esta linha de pesquisa.

A mensagem deste artigo não é, de forma alguma, de que as políticas alternativas devam se basear principalmente na ciência dura (N.T. ciências exatas e biológicas, do inglês “hard science”). No entanto, a ciência dura tem um papel a desempenhar. Esperemos que ela não seja mais deixada de lado.
<hr>
<br>
<strong>Salvador Pueyo</strong> <em>é pesquisador transdisciplinar do Departament de Biologia Evolutiva, Ecologia i Ciències Ambientals da Universitat de Barcelona.
</em>

*<span id="result_box" class="" lang="pt"><span class="">Um sistema multifractal é uma generalização de um sistema fractal em que um único expoente (a dimensão fractal) não é suficiente para descrever sua dinâmica; Em vez disso, é necessário um espectro contínuo de expoentes (o chamado espectro de singularidade).
Sistemas Multifractal são comuns na natureza e incluem o comprimento das linhas costeiras, as séries temporais do mercado de ações, as séries temporais do campo magnético do Sol, a dinâmica dos batimentos cardíacos, a marcha humana e as séries temporais de luminosidade natural.</span>

**<span id="result_box" class="" lang="pt"><span class="">O modelo baseado em agentes (MBA) é uma das classes de modelos computacionais que simula as ações e interações de agentes autônomos (entidades individuais ou coletivas, organizações ou grupos), em um dado contexto, com objetivo de avaliar seus efeitos no sistema como um todo.
</span></span>
<br>
<h3><strong>Referências</strong></h3>
[^1]: Pueyo, S. Ecological econophysics for degrowth. Sustainability 6, 3431–3483 (2014).

[^2]: D’Alissa, G., Demaria, F. & Kallis, G. (eds.) Degrowth: A Vocabulary for a New Era (Routledge, London, 2014).

[^3]: Chakraborti, A., Muni Toke, I., Patriarca, M. & Abergel, F. Econophysics review: I. Empirical facts. Quantitative Finance 11, 991–1012 (2011).

[^4]: Chakraborti, A., Muni Toke, I., Patriarca, M. & Abergel, F. Econophysics review: II.
Agent-based models. Quantitative Finance 11, 1013–1041 (2011).

[^5]: Waldrop, M. Complexity. The Emerging Science at the Edge of Order and Chaos (Simon & Schuster, New York, 1992).

[^6]: Pueyo, S. Is it a power law? the case of economic contractions. arXiv 1310.2567v1 [nlin.AO] (2013).

[^7]: Pueyo, S. Self-organised criticality and the response of wildland fires to climate change. Climatic Change 82, 131–161 (2007).

[^8]: Martinez-Alier, J. & Schlüpmann., K. Ecological Economics: Energy, Environment and
Society (Blackwell, Oxford, 1990).

[^9]: Yakovenko, V. M. & Rosser Jr, J. B. Colloquium: Statistical mechanics of money, wealth,
and income. Reviews of Modern Physics 81, 1703–1725 (2009).

[^10]: Rosales, F., Posadas, A. & Quiroz, R. Multifractal characterization of spatial income curdling: Theory and applications. Advances in Complex Systems 11, 861–874 (2008).

[^11]: Solé, R. V. Phase Transitions (Princeton University Press, 2011).

[^12]: Stanley, H. E. Scaling, universality, and renormalization: Three pillars of modern critical phenomena. Reviews of Modern Physics 71, S358–S366 (1999).

[^13]: Pueyo, S. Idiosyncrasy as an explanation for power laws in nature. In Corral, Á. et al. (eds.) Trends in Mathematics: Extended Abstracts Spring 2013, 69–74 (Springer, 2014).

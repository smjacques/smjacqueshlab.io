---
  title: "Sobre o site"
---
<hr>
  <br>
  
  Esse site foi desenvolvido usando R Markdown e todo o conteúdo utiliza licença Creative Commons Atribuição-CompartilhaIgual.
  
  *Os ícones são do Font Awesome e podem ser encontados em: 
  
  fontawesome.io/icons/
  
  www.shareicon.net
<br>

  <b>Sobre a licença usada no site:</b>
  
  <br>
> Atribuição-Compartilhamento pela mesma licença: Se você alterar, transformar ou criar outra obra em cima dessa, só poderá distribuir a obra derivada sob a mesma licença da obra original e creditar o autor original na atribuição.

<hr>
<center><rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0;" class="centerImage" src="https://licensebuttons.net/l/by-sa/4.0/88x31.png"></center>